package com.xiaopo.flying.suspensionbar.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xiaopo.flying.suspensionbar.R;

import java.util.List;

/**
 * Created by 顾博君 on 2017/3/10.
 */

public class SortAdapter2 extends RecyclerView.Adapter<SortAdapter2.SortHolder> {
    private List<SortModel> list = null;
    private Context mContext;
    public static final int TYPE_FIRST = 0;
    public static final int TYPE_OTHER = 1;

    public SortAdapter2(Context mContext, List<SortModel> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public SortHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new SortHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SortHolder holder, int position) {
        //根据position获取分类的首字母的Char ascii值
        int section = getSectionForPosition(position);
        final SortModel mContent = list.get(position);
        //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
        if (position == getPositionForSection(section)) {
            holder.tvLetter.setVisibility(View.VISIBLE);
            holder.tvLetter.setText(mContent.getSortLetters());
        } else {
            holder.tvLetter.setVisibility(View.GONE);
        }

        holder.tvTitle.setText(this.list.get(position).getName());
    }

    /**
     * 当ListView数据发生变化时,调用此方法来更新ListView
     *
     * @param list
     */
    public void updateListView(List<SortModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的Char ascii值
     */
    public int getSectionForPosition(int position) {
        if (list != null && list.size() > position)
            return list.get(position).getSortLetters().charAt(0);
        return 0;
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = list.get(i).getSortLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int getItemViewType(int position) {
        //根据position获取分类的首字母的Char ascii值
        int section = getSectionForPosition(position);
        if (position == getPositionForSection(section)) {
            return TYPE_FIRST;
        }
        return TYPE_OTHER;
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public static class SortHolder extends RecyclerView.ViewHolder {
        TextView tvLetter;
        TextView tvTitle;

        public SortHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.title);
            tvLetter = (TextView) itemView.findViewById(R.id.catalog);
        }
    }
}
